﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;

namespace DapperExtensions.TableQueryParameter
{
	public class TableQueryParameterAttribute : System.Attribute
	{
		public string TypeName;
		public List<DataColumn> Columns;

		//public TableQueryParameterAttribute(string typeName) :
		//	this(typeName, new string[] { })
		//{ }

		public TableQueryParameterAttribute(string typeName, string columnName = null) :
			this(typeName, new string[] { columnName })
		{ }
		
		public TableQueryParameterAttribute(string typeName, DataColumn column) :
			this(typeName, null, new DataColumn[] { column })
		{ }

		public TableQueryParameterAttribute(string typeName, IEnumerable<string> columnsName) :
			this(typeName, columnsName, null)
		{ }

		public TableQueryParameterAttribute(string typeName, IEnumerable<string> columnsName, IEnumerable<DataColumn> columns)
		{
			this.TypeName = typeName;
			IEnumerable<string> colNames = columnsName?.Where(n => !String.IsNullOrWhiteSpace(n)).ToArray();
			if (colNames != null && colNames.Any())
			{
				this.Columns = new List<DataColumn>();
				foreach (var colName in colNames)
				{
					var col = ((columns != null) ? columns.First(c => c.ColumnName == colName) : null) ?? new DataColumn(colName);
					col.Caption = col.ColumnName;
					this.Columns.Add(col);
				}
			} else
			{
				this.Columns = columns?.ToList();
			}
		}
	}
}
