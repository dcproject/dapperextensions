﻿using Dapper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Reflection;
using static Dapper.SqlMapper;

namespace DapperExtensions.TableQueryParameter
{
	public static class TableQueryParameterBilder
	{
		/// <summary>
		/// Auto create TVP parameters with columns from this types: bool, byte, sbyte, char, decimal, double, float, int, uint, long, ulong, short, ushort, string, Guid
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="queryParams"></param>
		/// <returns></returns>
		public static object Bild<T>(T queryParams)
		{
			var newQueryParams = new ExpandoObject();

			var type = typeof(T);
			var allProps = (type.GetProperties() ?? new PropertyInfo[0]);

			foreach(var prop in allProps)
			{
				if (string.IsNullOrWhiteSpace(prop?.Name)) { continue; }
				//Parameter value
				object propValue = prop.GetValue(queryParams);
				//TVP Parameter value
				TableQueryParameterAttribute tvpAttribute = GetTvpAttribute(prop);
				ICustomQueryParameter propTvpValue = (tvpAttribute == null) ? null :
					CreateTvp(propValue, prop?.PropertyType, tvpAttribute?.TypeName, tvpAttribute?.Columns);
				//set Parameter value or TVP Parameter value
				newQueryParams.TryAdd(prop.Name, propTvpValue ?? propValue);
			}
			return newQueryParams;
		}

		public static ICustomQueryParameter CreateTvp<T>(this IEnumerable<T> items, string tableTypeName = null)
		{
			return CreateTvp(items, typeof(T), tableTypeName);
		}

		private static TableQueryParameterAttribute GetTvpAttribute(PropertyInfo propertyInfo)
		{
			var tvpAttribute = propertyInfo?.GetCustomAttributes<TableQueryParameterAttribute>()?.FirstOrDefault();
			if (tvpAttribute == null)
			{
				tvpAttribute = propertyInfo?.PropertyType?.GetCustomAttributes<TableQueryParameterAttribute>()?.FirstOrDefault();
			}
			return tvpAttribute;
		}

		private static ICustomQueryParameter CreateTvp(dynamic items, Type type, string tableTypeName, IEnumerable<DataColumn> columns = null)
		{
			IEnumerable array = items as IEnumerable;
			if (string.IsNullOrWhiteSpace(tableTypeName) || (array == null && !IsEnumerable(type)))
			{
				return null;
			}
			var table = new DataTable();

			#region Columns Set
			Type itemType = null;
			if (type != null && typeof(IEnumerable).IsAssignableFrom(type))
			{
				if (type.IsGenericType)
				{
					itemType = type.GetGenericArguments()[0];
				}
				else if (array != null)
                {
                    foreach (var item in array)
                    {
                        itemType = item.GetType();
                        break;
                    }
                }
                else
                {
                    throw new NotImplementedException("");
                }
					
			}
			IEnumerable<DataColumn> tableCols = DataColumnsBild(itemType, columns);

			table.Columns.AddRange((tableCols ?? (tableCols = new DataColumn[] { })).ToArray());

			#endregion Columns Set

			#region Rows Set
			if (array != null && table.Columns.Count > 0)
				foreach (var item in array)
				{
					var row = table.NewRow();
					itemType = item.GetType();

					var colType = GetTypeByColumn(itemType);
					if (AloweTypeByColumn(colType))
					{
						var col = tableCols.First(c => c.DataType == colType);
						if (col != null)
						{
							row[col.ColumnName] = item;
						}
						table.Rows.Add(row);
					}
					else if (!typeof(IEnumerable).IsAssignableFrom(itemType))
					{
						var itemProps = itemType.GetProperties();
						foreach (DataColumn col in table.Columns)
						{
							try
							{
								var prop = itemProps.First(p => p.Name == col.ColumnName);
								row[col.ColumnName] = prop?.GetValue(item) ?? DBNull.Value;
							}
							catch (Exception ex)
							{

							}
						}
						table.Rows.Add(row);
					}
				}
			#endregion Rows Set

			if (table.Columns.Count > 0)
			{
				table.EndLoadData();
				return table.AsTableValuedParameter(tableTypeName);
			}
			return null;
		}

		private static IEnumerable<DataColumn> DataColumnsBild(Type type, IEnumerable<DataColumn> column)
		{
			var validColumns = column?.Where(c => !string.IsNullOrWhiteSpace(c.ColumnName) && c.DataType != defaultColumnType).ToList() ?? new List<DataColumn>();
			var invalidColumns = column?.Where(c => !string.IsNullOrWhiteSpace(c.ColumnName) && c.DataType == defaultColumnType).ToList() ?? new List<DataColumn>();
			var autoCreateNewColumn = !validColumns.Any();
			if (type == null || (!autoCreateNewColumn && !invalidColumns.Any()))
			{
				return validColumns;
			}
			//(AloweTypeByColumn(type) && ) || typeof(IEnumerable).IsAssignableFrom(type)			
			Type colType = GetTypeByColumn(type);
			// if columns is only one and the type alowe by column:
			if (AloweTypeByColumn(colType) && !validColumns.Any(c => c.DataType == colType) && invalidColumns.Count() == 1)
			{
				invalidColumns.First().DataType = colType;
				validColumns.AddRange(invalidColumns);
			}
			// if the type is not emplimant IEnumerable then create coluns from the type:
			else if (!typeof(IEnumerable).IsAssignableFrom(type))
			{
				var props = type.GetProperties();
				foreach (var prop in props)
				{
					colType = GetTypeByColumn(prop.PropertyType);
					if (AloweTypeByColumn(colType) && !validColumns.Any(c => c.ColumnName == prop.Name))
					{
						try
						{
							var col = invalidColumns?.Find(c => c.ColumnName == prop.Name)
								?? (autoCreateNewColumn ? new DataColumn()
								{
									AllowDBNull = true,
									ColumnName = prop.Name,
									Caption = prop.Name
								} : null);
							if (col != null)
							{
								col.DataType = colType;
								validColumns.Add(col);
							}
						}
						catch (Exception ex)
						{

						}
					}
				}
			}
			return validColumns;
		}

		private static Type defaultColumnType = new DataColumn().DataType;
		static string[] aloweType = (new List<Type>() {
			typeof(bool),typeof(byte),typeof(sbyte),typeof(char),typeof(decimal),typeof(double),typeof(float),typeof(int),typeof(uint),typeof(long),typeof(ulong),typeof(short),typeof(ushort),typeof(string),typeof(Guid)
		}).Select(t => t.Name).ToArray();

		private static bool AloweTypeByColumn(Type type)
		{
			var typeName = type?.Name;
			return !String.IsNullOrWhiteSpace(typeName) && aloweType.Contains(typeName);
		}

		private static Type GetTypeByColumn(Type type)
		{
			if (type.IsEnum)
			{
				return Enum.GetUnderlyingType(type);
			}
			return Nullable.GetUnderlyingType(type) ?? type;
		}

		private static bool IsEnumerable(Type type)
		{
			return type != null && typeof(IEnumerable).IsAssignableFrom(type);
		}

	}
}
